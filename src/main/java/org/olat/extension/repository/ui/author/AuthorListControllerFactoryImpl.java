package org.olat.extension.repository.ui.author;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.impl.Form;
import org.olat.core.gui.control.WindowControl;
import org.olat.repository.model.SearchAuthorRepositoryEntryViewParams;
import org.olat.repository.ui.author.AuthorListConfiguration;
import org.olat.repository.ui.author.AuthorListController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Martin Schraner
 * @since 11.4
 */
@Component
public class AuthorListControllerFactoryImpl implements AuthorListControllerFactory {

	private final AuthoringEntryRowFactory authoringEntryRowFactory;

	@Autowired
	public AuthorListControllerFactoryImpl(AuthoringEntryRowFactory authoringEntryRowFactory) {
		this.authoringEntryRowFactory = authoringEntryRowFactory;
	}

	@Override
	public AuthorListController create(UserRequest ureq, WindowControl wControl,
		SearchAuthorRepositoryEntryViewParams searchParams, AuthorListConfiguration configuration) {
		return new AuthorListController(ureq, wControl, searchParams, configuration,
			authoringEntryRowFactory);
	}

	@Override
	public AuthorListController create(UserRequest ureq, WindowControl wControl, Form rootForm,
		SearchAuthorRepositoryEntryViewParams searchParams, AuthorListConfiguration configuration) {
		return new AuthorListController(ureq, wControl, rootForm, searchParams, configuration,
			authoringEntryRowFactory);
	}
}
