package org.olat.extension.repository.ui.author;

import org.olat.repository.RepositoryEntryAuthorView;
import org.olat.repository.ui.author.AuthoringEntryRow;
import org.springframework.stereotype.Component;

/**
 * @author Martin Schraner
 * @since 11.4
 */
@Component
public class AuthoringEntryRowFactoryImpl implements AuthoringEntryRowFactory {

	@Override
	public AuthoringEntryRow create(RepositoryEntryAuthorView view, String fullnameAuthor) {
		return new AuthoringEntryRow(view, fullnameAuthor);
	}
}
