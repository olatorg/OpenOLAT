package org.olat.extension.user;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.id.Identity;
import org.olat.user.ProfileAndHomePageEditController;
import org.springframework.stereotype.Component;

/**
 * @author Martin Schraner
 * @since 15.5
 */
@Component
public class ProfileAndHomePageEditControllerFactoryImpl implements
	ProfileAndHomePageEditControllerFactory {

	@Override
	public ProfileAndHomePageEditController create(UserRequest userRequest,
		WindowControl windowControl) {
		return new ProfileAndHomePageEditController(userRequest, windowControl);
	}

	@Override
	public ProfileAndHomePageEditController create(
		UserRequest userRequest, WindowControl windowControl, Identity identityToModify,
		boolean isAdministrativeUser) {
		return new ProfileAndHomePageEditController(userRequest, windowControl, identityToModify,
			isAdministrativeUser);
	}
}