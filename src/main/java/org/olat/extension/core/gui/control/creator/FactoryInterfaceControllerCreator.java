package org.olat.extension.core.gui.control.creator;

import org.olat.core.CoreSpringFactory;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.gui.control.creator.AutoCreator;
import org.olat.core.logging.OLATRuntimeException;

/**
 * @author Martin Schraner
 * @since 15.5
 */
public class FactoryInterfaceControllerCreator extends AutoCreator {

	private String userRequestWindowControlControllerFactoryInterfaceName;

	@Override
	public Controller createController(UserRequest ureq, WindowControl wControl) {
		if (userRequestWindowControlControllerFactoryInterfaceName == null) {
			throw new OLATRuntimeException(
				"userRequestWindowControlControllerFactoryInterfaceName is null!");
		}

		// Get factory interface class object from class name
		Class<?> factoryInterfaceClassObject;
		try {
			factoryInterfaceClassObject =
				Thread.currentThread()
					.getContextClassLoader()
					.loadClass(userRequestWindowControlControllerFactoryInterfaceName);
		} catch (ClassNotFoundException e) {
			throw new OLATRuntimeException(userRequestWindowControlControllerFactoryInterfaceName
				+ ": class not found!");
		}

		/*
		 * Load corresponding Spring bean (works also in case of @Primary annotated beans)
		 * Attention! It won't be possible to load corresponding bean in case
		 * package org.olat.extension is not scanned for components anymore
		 * (e.g. when property openolat.extension.enabled=false is set via OpenOlat Starter).
		 */
		Object factoryImplementation;
		try {
			factoryImplementation = CoreSpringFactory.getContext().getBean(factoryInterfaceClassObject);
		} catch (Exception e) {
			throw new OLATRuntimeException(
				"Could not find bean implementation for " + factoryInterfaceClassObject.getName());
		}

		// Cast to UserRequestWindowControlControllerFactory
		UserRequestWindowControlControllerFactory userRequestWindowControlControllerFactory;
		if (factoryImplementation instanceof UserRequestWindowControlControllerFactory) {
			userRequestWindowControlControllerFactory
				= (UserRequestWindowControlControllerFactory) factoryImplementation;
		} else {
			throw new OLATRuntimeException(
				userRequestWindowControlControllerFactoryInterfaceName
					+ ": not instance of UserRequestWindowControlControllerFactory!");
		}

		// Create controller
		return userRequestWindowControlControllerFactory.create(ureq, wControl);
	}

	public void setUserRequestWindowControlContollerFactoryIntefaceName(
		String userRequestWindowControlContollerFactoryIntefaceName) {
		this.userRequestWindowControlControllerFactoryInterfaceName
			= userRequestWindowControlContollerFactoryIntefaceName;
	}

	@Override
	public String getClassName() {
		return userRequestWindowControlControllerFactoryInterfaceName;
	}
}