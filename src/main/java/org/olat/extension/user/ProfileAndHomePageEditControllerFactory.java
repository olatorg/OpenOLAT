package org.olat.extension.user;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.id.Identity;
import org.olat.extension.core.gui.control.creator.UserRequestWindowControlControllerFactory;
import org.olat.user.ProfileAndHomePageEditController;

/**
 * @author Martin Schraner
 * @since 15.5
 */
public interface ProfileAndHomePageEditControllerFactory extends
	UserRequestWindowControlControllerFactory {

	ProfileAndHomePageEditController create(UserRequest userRequest, WindowControl windowControl);

	ProfileAndHomePageEditController create(
		UserRequest userRequest, WindowControl windowControl, Identity identityToModify,
		boolean isAdministrativeUser);
}