package org.olat.repository.manager;

import org.olat.core.logging.OLATRuntimeException;

/**
 * @author Martin Schraner
 * @since 11.4
 */
public class RepositoryEntryDeletionException extends Exception {

	public RepositoryEntryDeletionException(String message) {
		super(message);
	}
}
