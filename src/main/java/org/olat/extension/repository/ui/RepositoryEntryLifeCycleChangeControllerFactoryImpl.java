package org.olat.extension.repository.ui;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.control.WindowControl;
import org.olat.repository.RepositoryEntry;
import org.olat.repository.RepositoryEntrySecurity;
import org.olat.repository.handlers.RepositoryHandler;
import org.olat.repository.ui.RepositoryEntryLifeCycleChangeController;
import org.springframework.stereotype.Component;

/**
 * @author Martin Schraner
 * @since 15.5
 */
@Component
public class RepositoryEntryLifeCycleChangeControllerFactoryImpl
	implements RepositoryEntryLifeCycleChangeControllerFactory {

	@Override
	public RepositoryEntryLifeCycleChangeController create(
		UserRequest userRequest,
		WindowControl windowControl,
		RepositoryEntrySecurity repositoryEntrySecurity,
		RepositoryHandler repositoryHandler,
		RepositoryEntry repositoryEntry) {

		return new RepositoryEntryLifeCycleChangeController(
			userRequest,
			windowControl,
			repositoryEntry,
			repositoryEntrySecurity,
			repositoryHandler);
	}
}
