package org.olat.extension.repository.ui.list;

import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableColumnModel;
import org.olat.repository.ui.list.DefaultRepositoryEntryDataSource;
import org.olat.repository.ui.list.RepositoryEntryDataModel;

/**
 * @author Martin Schraner
 * @since 15.5
 */
public interface RepositoryEntryDataModelFactory {

	RepositoryEntryDataModel create(
		DefaultRepositoryEntryDataSource defaultRepositoryEntryDataSource,
		FlexiTableColumnModel flexiTableColumnModel);
}
