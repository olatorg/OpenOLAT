package org.olat.repository.manager.coursequery;

import org.olat.repository.RepositoryEntryAuthorViewResults;
import org.olat.repository.model.SearchAuthorRepositoryEntryViewParams;

/**
 * @author Martin Schraner
 * @since 15.5
 */
public interface RepositoryEntryAuthorQuery {

	int countViews(SearchAuthorRepositoryEntryViewParams params);

	RepositoryEntryAuthorViewResults searchViews(
		SearchAuthorRepositoryEntryViewParams params, int firstResult, int maxResults);
}
