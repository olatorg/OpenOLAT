/*
 * Copyright 2025 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.group.manager;

import org.olat.group.BusinessGroup;
import org.springframework.beans.factory.ObjectProvider;

/**
 * @author Martin Schraner
 * @since 15.5
 */
public interface BusinessGroupDeletionListener {

  static void onBeforeDelete(ObjectProvider<BusinessGroupDeletionListener> listeners,
      BusinessGroup businessGroup) {
    listeners.orderedStream().forEach(listener -> listener.onBeforeDelete(businessGroup));
  }

  void onBeforeDelete(BusinessGroup businessGroup);
}