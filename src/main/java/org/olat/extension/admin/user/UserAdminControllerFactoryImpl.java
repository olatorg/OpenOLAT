package org.olat.extension.admin.user;

import org.olat.admin.user.UserAdminController;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.stack.TooledStackedPanel;
import org.olat.core.gui.control.WindowControl;
import org.olat.core.id.Identity;
import org.springframework.stereotype.Component;

/**
 * @author Martin Schraner
 * @since 15.5
 */
@Component
public class UserAdminControllerFactoryImpl implements UserAdminControllerFactory {

	@Override
	public UserAdminController create(
		UserRequest userRequest, WindowControl windowControl, TooledStackedPanel stackPanel,
		Identity identity) {
		return new UserAdminController(userRequest, windowControl, null, stackPanel, identity);
	}
}