/*
 * Copyright 2025 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.repository.ui.list;

import org.olat.core.gui.components.velocity.VelocityContainer;
import org.olat.core.gui.control.controller.BasicController;
import org.olat.core.gui.translator.Translator;

/**
 * @author Martin Schraner
 * @since 14.2
 */
public class RepositoryEntryRowVelocityContainer extends VelocityContainer {
  public RepositoryEntryRowVelocityContainer(
      String pagePath, Translator translator, BasicController caller) {
    super(null, "vc_row_1", pagePath, translator, caller);
    setDomReplacementWrapperRequired(false);
  }
}
