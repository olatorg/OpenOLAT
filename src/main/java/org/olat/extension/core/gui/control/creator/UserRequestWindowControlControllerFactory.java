package org.olat.extension.core.gui.control.creator;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.WindowControl;

/**
 * @author Martin Schraner
 * @since 15.5
 */
public interface UserRequestWindowControlControllerFactory {

	Controller create(UserRequest ureq, WindowControl wControl);
}