package org.olat.extension.course.run;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.control.WindowControl;
import org.olat.course.run.CourseRuntimeController;
import org.olat.repository.RepositoryEntry;
import org.olat.repository.RepositoryEntrySecurity;
import org.olat.repository.ui.RepositoryEntryRuntimeController;
import org.springframework.stereotype.Service;

/**
 * @author Martin Schraner
 * @since 15.4
 */
@Service
public class CourseRuntimeControllerFactoryImpl implements CourseRuntimeControllerFactory {

	@Override
	public CourseRuntimeController create(UserRequest userRequest, WindowControl windowControl,
		RepositoryEntry repositoryEntry,
		RepositoryEntrySecurity repositoryEntrySecurity,
		RepositoryEntryRuntimeController.RuntimeControllerCreator runtimeControllerCreator,
		boolean offerBookmark, boolean showCourseConfigLink) {
		return new CourseRuntimeController(userRequest, windowControl, null, repositoryEntry,
			repositoryEntrySecurity, runtimeControllerCreator,
			offerBookmark, showCourseConfigLink);
	}
}
