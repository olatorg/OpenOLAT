package org.olat.extension.course.run;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.control.WindowControl;
import org.olat.course.run.CourseRuntimeController;
import org.olat.repository.RepositoryEntry;
import org.olat.repository.RepositoryEntrySecurity;
import org.olat.repository.ui.RepositoryEntryRuntimeController;

/**
 * @author Martin Schraner
 * @since 15.4
 */
public interface CourseRuntimeControllerFactory {

	CourseRuntimeController create(UserRequest userRequest, WindowControl windowControl,
		RepositoryEntry repositoryEntry, RepositoryEntrySecurity repositoryEntrySecurity,
		RepositoryEntryRuntimeController.RuntimeControllerCreator runtimeControllerCreator,
		boolean offerBookmark, boolean showCourseConfigLink);
}
