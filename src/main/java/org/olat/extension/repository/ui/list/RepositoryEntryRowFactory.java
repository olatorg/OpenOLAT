package org.olat.extension.repository.ui.list;

import java.util.Locale;
import org.olat.repository.RepositoryEntryMyView;
import org.olat.repository.ui.list.RepositoryEntryRow;

/**
 * @author Martin Schraner
 * @since 15.5
 */
public interface RepositoryEntryRowFactory {

	RepositoryEntryRow create(RepositoryEntryMyView repositoryEntryMyView, Locale locale);
}