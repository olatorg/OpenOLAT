/*
 * Copyright 2025 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.repository.ui.list;

import org.olat.repository.ui.list.RepositoryEntryListController;

/**
 * @author Christian Schweizer
 * @since 19.1
 */
public interface RepositoryEntryRowVelocityContainerFactory {

  RepositoryEntryRowVelocityContainer create(RepositoryEntryListController caller);
}
