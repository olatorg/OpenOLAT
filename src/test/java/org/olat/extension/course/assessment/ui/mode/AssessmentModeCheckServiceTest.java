/*
 * <a href=“https://www.OpenOlat.org“>
 * OpenOlat - Online Learning and Training</a><br>
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); <br>
 * you may not use this file except in compliance with the License.<br>
 * You may obtain a copy of the License at the
 * <a href="https://www.apache.org/licenses/LICENSE-2.0">Apache homepage</a>
 * <p>
 * Unless required by applicable law or agreed to in writing,<br>
 * software distributed under the License is distributed on an "AS IS" BASIS, <br>
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. <br>
 * See the License for the specific language governing permissions and <br>
 * limitations under the License.
 * <p>
 * Initial code contributed and copyrighted by<br>
 * 2024 by Multimedia- & E-Learning Services (MELS),<br>
 * University of Zurich, Switzerland, https://www.uzh.ch
 * <p>
 */
package org.olat.extension.course.assessment.ui.mode;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.olat.core.gui.UserRequest;
import org.olat.course.assessment.AssessmentMode;
import org.olat.course.assessment.model.TransientAssessmentMode;
import org.olat.repository.RepositoryEntry;
import org.olat.resource.OLATResource;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

/**
 * @author Christian Schweizer
 * @version 18.2
 */
@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("java:S1117")
public class AssessmentModeCheckServiceTest {

	@Mock
	private AssessmentMode assessmentMode;
	@Mock
	private RepositoryEntry entry;
	@Mock
	private OLATResource resource;
	@Mock
	private UserRequest userRequest;

	@Before
	public void setUp() {
		when(assessmentMode.getRepositoryEntry()).thenReturn(entry);
		when(entry.getOlatResource()).thenReturn(resource);
	}

	@Test
	public void checkAssessmentMode_withAllTrue() {
		try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext()) {
			context.register(TestConfiguration.class);
			context.registerBean("check1", AssessmentModeCheck.class,
				() -> (userRequest, assessmentMode, sb) -> true);
			context.registerBean("check2", AssessmentModeCheck.class,
				() -> (userRequest, assessmentMode, sb) -> true);
			context.refresh();
			assertThat(context.getBean(AssessmentModeCheckService.class)).isNotNull();
			assertThat(context.getBean(AssessmentModeCheckService.class)
				.checkAssessmentMode(userRequest, new TransientAssessmentMode(assessmentMode),
					new StringBuilder())).isTrue();
		}
	}

	@Test
	public void checkAssessmentMode_withFirstFalse() {
		try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext()) {
			context.register(TestConfiguration.class);
			context.registerBean("check1", AssessmentModeCheck.class,
				() -> (userRequest, assessmentMode, sb) -> false);
			context.registerBean("check2", AssessmentModeCheck.class,
				() -> (userRequest, assessmentMode, sb) -> true);
			context.refresh();
			assertThat(context.getBean(AssessmentModeCheckService.class)).isNotNull();
			assertThat(context.getBean(AssessmentModeCheckService.class)
				.checkAssessmentMode(userRequest, new TransientAssessmentMode(assessmentMode),
					new StringBuilder())).isFalse();
		}
	}

	@Test
	public void checkAssessmentMode_withLastFalse() {
		try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext()) {
			context.register(TestConfiguration.class);
			context.registerBean("check1", AssessmentModeCheck.class,
				() -> (userRequest, assessmentMode, sb) -> true);
			context.registerBean("check2", AssessmentModeCheck.class,
				() -> (userRequest, assessmentMode, sb) -> false);
			context.refresh();
			assertThat(context.getBean(AssessmentModeCheckService.class)).isNotNull();
			assertThat(context.getBean(AssessmentModeCheckService.class)
				.checkAssessmentMode(userRequest, new TransientAssessmentMode(assessmentMode),
					new StringBuilder())).isFalse();
		}
	}

	static class TestConfiguration {

		@Bean
		public AssessmentModeCheckService assessmentModeCheckService(
			ObjectProvider<AssessmentModeCheck> assessmentModeChecks) {
			return new AssessmentModeCheckServiceImpl(assessmentModeChecks);
		}
	}
}