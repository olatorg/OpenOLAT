package org.olat.repository.manager.coursequery;

import java.util.List;
import org.olat.repository.RepositoryEntryMyView;
import org.olat.repository.model.SearchMyRepositoryEntryViewParams;

/**
 * This interface is required in order that the course search can be extended with other sources. An
 * example is the UZH Campuskurs source: campus courses, that were synchronized with a third party
 * database but have not yet been created, should be found and listed.
 *
 * @author sev26
 * @since 11.4
 */
public interface MyCourseRepositoryQuery {

	int countViews(SearchMyRepositoryEntryViewParams params);

	List<RepositoryEntryMyView> searchViews(
		SearchMyRepositoryEntryViewParams params, int firstResult, int maxResults);
}