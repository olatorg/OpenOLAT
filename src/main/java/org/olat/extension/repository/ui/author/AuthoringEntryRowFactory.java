package org.olat.extension.repository.ui.author;

import org.olat.repository.RepositoryEntryAuthorView;
import org.olat.repository.ui.author.AuthoringEntryRow;

/**
 * @author Martin Schraner
 * @since 11.4
 */
public interface AuthoringEntryRowFactory {

	AuthoringEntryRow create(RepositoryEntryAuthorView view, String fullnameAuthor);
}
