/*
 * <a href=“https://www.OpenOlat.org“>
 * OpenOlat - Online Learning and Training</a><br>
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); <br>
 * you may not use this file except in compliance with the License.<br>
 * You may obtain a copy of the License at the
 * <a href="https://www.apache.org/licenses/LICENSE-2.0">Apache homepage</a>
 * <p>
 * Unless required by applicable law or agreed to in writing,<br>
 * software distributed under the License is distributed on an "AS IS" BASIS, <br>
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. <br>
 * See the License for the specific language governing permissions and <br>
 * limitations under the License.
 * <p>
 * Initial code contributed and copyrighted by<br>
 * 2024 by Multimedia- & E-Learning Services (MELS),<br>
 * University of Zurich, Switzerland, https://www.uzh.ch
 * <p>
 */
package org.olat.extension.course.assessment.ui.mode;

import java.util.concurrent.atomic.AtomicBoolean;
import org.olat.core.gui.UserRequest;
import org.olat.course.assessment.model.TransientAssessmentMode;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.stereotype.Service;

/**
 * @author Christian Schweizer
 * @since 18.2
 */
@Service
public class AssessmentModeCheckServiceImpl implements AssessmentModeCheckService {

	private final ObjectProvider<AssessmentModeCheck> assessmentModeChecks;

	public AssessmentModeCheckServiceImpl(
		ObjectProvider<AssessmentModeCheck> assessmentModeChecks) {
		this.assessmentModeChecks = assessmentModeChecks;
	}

	@Override
	public boolean checkAssessmentMode(UserRequest userRequest,
		TransientAssessmentMode assessmentMode,
		StringBuilder errors) {
		AtomicBoolean allowed = new AtomicBoolean(true);
		assessmentModeChecks.orderedStream().forEach(
			assessmentModeCheck -> allowed.compareAndSet(true,
				assessmentModeCheck.check(userRequest, assessmentMode, errors)));
		return allowed.get();
	}
}
