/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.extension.core.commons.services.analytics.spi;

import org.springframework.beans.factory.ObjectProvider;

/**
 * This interface is responsible to add custom features to the <a
 * href="https://developer.matomo.org/guides/tracking-javascript-guide">Matomo tracking code</a>.
 * <p>
 * An example:
 * <blockquote><pre>
 *   public class AddEnableHeartBeatTimerFeature implements MatomoTrackingCustomizer {
 *
 *     public void addFeature(StringBuilder features) {
 *       // accurately measure the time spent in the visit return
 *       features.append("_paq.push(['enableHeartBeatTimer']);\r");
 *     }
 *   }
 * </pre></blockquote>
 *
 * @author Christian Schweizer
 * @since 18.2
 */
public interface MatomoTrackingFeature {

	static String getTrackingCode(
		ObjectProvider<MatomoTrackingFeature> trackingFeatures) {
		StringBuilder additionalFeatures = new StringBuilder();
		trackingFeatures.orderedStream()
			.forEach(matomoTrackingFeature -> matomoTrackingFeature.addFeature(additionalFeatures));
		return additionalFeatures.toString();
	}

	void addFeature(StringBuilder features);
}
