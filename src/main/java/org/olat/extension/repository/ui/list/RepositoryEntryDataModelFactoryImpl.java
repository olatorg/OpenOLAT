package org.olat.extension.repository.ui.list;

import org.olat.core.gui.components.form.flexible.impl.elements.table.FlexiTableColumnModel;
import org.olat.repository.ui.list.DefaultRepositoryEntryDataSource;
import org.olat.repository.ui.list.RepositoryEntryDataModel;
import org.springframework.stereotype.Component;

/**
 * @author Martin Schraner
 * @since 15.5
 */
@Component
public class RepositoryEntryDataModelFactoryImpl implements RepositoryEntryDataModelFactory {
	
	@Override
	public RepositoryEntryDataModel create(
		DefaultRepositoryEntryDataSource defaultRepositoryEntryDataSource, 
		FlexiTableColumnModel flexiTableColumnModel) {
		return new RepositoryEntryDataModel(defaultRepositoryEntryDataSource, flexiTableColumnModel);
	}
}
