/*
 * Copyright 2024 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.olat.core.commons.services.doceditor.onlyoffice.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.olat.core.commons.services.doceditor.onlyoffice.Customization;

/**
 * @author Mateja Culjak
 * @since 18.2
 */
@JsonInclude(Include.NON_NULL)
public class CustomizationImpl implements Customization {

	private boolean plugins;

	@Override
	public boolean getPlugins() {
		return plugins;
	}

	public void setPlugins(boolean plugins) {
		this.plugins = plugins;
	}
}