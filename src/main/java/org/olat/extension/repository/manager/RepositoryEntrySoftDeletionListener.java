package org.olat.extension.repository.manager;

import org.olat.core.id.Identity;
import org.olat.repository.RepositoryEntry;
import org.olat.repository.manager.RepositoryEntryDeletionException;
import org.springframework.beans.factory.ObjectProvider;

/**
 * @author Martin Schraner
 * @since 15.5
 */
public interface RepositoryEntrySoftDeletionListener {

	static void onBeforeDelete(ObjectProvider<RepositoryEntrySoftDeletionListener> listeners,
		RepositoryEntry repositoryEntry, Identity deletedBy)
		throws RepositoryEntryDeletionException {
		for (RepositoryEntrySoftDeletionListener listener : listeners) {
			listener.onBeforeDelete(repositoryEntry, deletedBy);
		}
	}

	static void onAfterDelete(ObjectProvider<RepositoryEntrySoftDeletionListener> listeners,
		RepositoryEntry repositoryEntry, Identity deletedBy)
		throws RepositoryEntryDeletionException {
		for (RepositoryEntrySoftDeletionListener listener : listeners) {
			listener.onAfterDelete(repositoryEntry, deletedBy);
		}
	}

	void onBeforeDelete(RepositoryEntry repositoryEntry, Identity deletedBy)
		throws RepositoryEntryDeletionException;

	void onAfterDelete(RepositoryEntry repositoryEntry, Identity deletedBy)
		throws RepositoryEntryDeletionException;
}