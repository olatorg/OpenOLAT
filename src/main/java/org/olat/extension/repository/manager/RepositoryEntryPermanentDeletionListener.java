package org.olat.extension.repository.manager;

import org.olat.core.id.Identity;
import org.olat.repository.RepositoryEntry;
import org.olat.repository.manager.RepositoryEntryDeletionException;
import org.springframework.beans.factory.ObjectProvider;

/**
 * @author Martin Schraner
 * @since 15.5
 */
public interface RepositoryEntryPermanentDeletionListener {

	static void onBeforeDelete(ObjectProvider<RepositoryEntryPermanentDeletionListener> listeners,
		RepositoryEntry entry, Identity deletedBy) throws RepositoryEntryDeletionException {
		for (RepositoryEntryPermanentDeletionListener listener : listeners) {
			listener.onBeforeDelete(entry, deletedBy);
		}
	}

	static void onAfterDelete(
		ObjectProvider<RepositoryEntryPermanentDeletionListener> listeners, RepositoryEntry entry,
		Identity deletedBy)
		throws RepositoryEntryDeletionException {
		for (RepositoryEntryPermanentDeletionListener listener : listeners) {
			listener.onAfterDelete(entry, deletedBy);
		}
	}

	void onBeforeDelete(RepositoryEntry repositoryEntry, Identity deletedBy)
		throws RepositoryEntryDeletionException;

	void onAfterDelete(RepositoryEntry repositoryEntry, Identity deletedBy)
		throws RepositoryEntryDeletionException;
}