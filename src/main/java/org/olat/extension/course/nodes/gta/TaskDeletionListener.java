package org.olat.extension.course.nodes.gta;

import java.util.List;

/**
 * @author Martin Schraner
 * @since 15.5
 */
public interface TaskDeletionListener {

	void onBeforeDeleteTasksByTaskIdIn(List<Long> taskIds);
	
	void onBeforeDeleteTasksByTaskListId(long taskListId);
}
