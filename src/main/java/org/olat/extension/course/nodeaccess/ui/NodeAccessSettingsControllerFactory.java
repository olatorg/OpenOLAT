package org.olat.extension.course.nodeaccess.ui;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.control.WindowControl;
import org.olat.course.nodeaccess.ui.NodeAccessSettingsController;
import org.olat.repository.RepositoryEntry;

/**
 * @author Martin Schraner
 * @since 15.4
 */
public interface NodeAccessSettingsControllerFactory {

	NodeAccessSettingsController create(
		UserRequest userRequest,
		WindowControl windowControl,
		RepositoryEntry repositoryEntry,
		boolean readOnly);
}
