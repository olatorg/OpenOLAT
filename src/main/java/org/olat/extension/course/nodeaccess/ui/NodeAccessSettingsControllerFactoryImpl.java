package org.olat.extension.course.nodeaccess.ui;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.control.WindowControl;
import org.olat.course.nodeaccess.ui.NodeAccessSettingsController;
import org.olat.repository.RepositoryEntry;
import org.springframework.stereotype.Component;

/**
 * @author Martin Schraner
 * @since 15.4
 */
@Component
public class NodeAccessSettingsControllerFactoryImpl implements
	NodeAccessSettingsControllerFactory {

	@Override
	public NodeAccessSettingsController create(
		UserRequest userRequest,
		WindowControl windowControl,
		RepositoryEntry repositoryEntry,
		boolean readOnly) {

		return new NodeAccessSettingsController(
			userRequest,
			windowControl,
			repositoryEntry,
			readOnly);
	}
}
