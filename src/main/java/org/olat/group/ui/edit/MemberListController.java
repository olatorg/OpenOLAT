/**
 * <a href="http://www.openolat.org">
 * OpenOLAT - Online Learning and Training</a><br>
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); <br>
 * you may not use this file except in compliance with the License.<br>
 * You may obtain a copy of the License at the
 * <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache homepage</a>
 * <p>
 * Unless required by applicable law or agreed to in writing,<br>
 * software distributed under the License is distributed on an "AS IS" BASIS, <br>
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. <br>
 * See the License for the specific language governing permissions and <br>
 * limitations under the License.
 * <p>
 * Initial code contributed and copyrighted by<br>
 * frentix GmbH, http://www.frentix.com
 * <p>
 */
package org.olat.group.ui.edit;

import java.util.List;
import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.FormItemContainer;
import org.olat.core.gui.components.stack.TooledStackedPanel;
import org.olat.core.gui.control.Controller;
import org.olat.core.gui.control.WindowControl;
import org.olat.group.BusinessGroup;
import org.olat.group.BusinessGroupManagedFlag;
import org.olat.group.ui.main.AbstractMemberListController;
import org.olat.group.ui.main.CourseMembership;
import org.olat.group.ui.main.MemberListSecurityCallback;
import org.olat.group.ui.main.MemberRow;
import org.olat.group.ui.main.SearchMembersParams;

/**
 * The list of members specific to the business groups.
 * 
 * @author srosse, stephane.rosse@frentix.com, http://www.frentix.com
 */
public class MemberListController extends AbstractMemberListController {
	
	private final SearchMembersParams searchParams;
	
	public MemberListController(UserRequest ureq, WindowControl wControl, TooledStackedPanel stackPanel,
			BusinessGroup group, SearchMembersParams searchParams, MemberListSecurityCallback secCallback) {
		super(ureq, wControl, group, "all_member_list", secCallback, stackPanel);
		this.searchParams = searchParams;
	}

	@Override
	protected void initForm(FormItemContainer formLayout, Controller listener, UserRequest ureq) {
		super.initForm(formLayout, listener, ureq);
		// If both the 'membersManagement' and the 'excludeGroupCoachesFromMembersManagement' flags are set for a group, 
		// group members with group coach as group role can be edited or can be removed. Therefore we have to make the
		// edit and remove buttons visible in that case.
		if (isMembersManagementFlagSet() && isExcludeGroupCoachesFromMembersManagementFlagSet()) {
			editButton.setVisible(true);
			removeButton.setVisible(true);
		}
	}

	private boolean isMembersManagementFlagSet() {
		return businessGroup != null && BusinessGroupManagedFlag.isManaged(
			businessGroup, BusinessGroupManagedFlag.membersmanagement);
	}

	private boolean isExcludeGroupCoachesFromMembersManagementFlagSet() {
		return businessGroup != null && BusinessGroupManagedFlag.isManaged(
			businessGroup.getManagedFlags(), BusinessGroupManagedFlag.excludeGroupCoachesFromMembersmanagement);
	}

	@Override
	protected void doConfirmRemoveMembers(UserRequest ureq, List<MemberRow> members) {
		// If both the 'membersManagement' and the 'excludeGroupCoachesFromMembersManagement' are set for a group, 
		// we have to check if all group members marked for removal have the group coach role (and no other role),
		// since only group coaches can be removed from the group. If this is not the case, an error message gets
		// displayed.
		if (isMembersManagementFlagSet() && isExcludeGroupCoachesFromMembersManagementFlagSet()) {
			boolean membersWithParticipantRoleSelected = false;
			boolean membersWithCoachAndParticipantRoleSelected = false;
			for (MemberRow member : members) {
				CourseMembership courseMembership = member.getMembership();
				if (courseMembership.isBusinessGroupCoach() && courseMembership.isBusinessGroupParticipant()) {
					membersWithCoachAndParticipantRoleSelected = true;
				} else if (courseMembership.isBusinessGroupParticipant()) {
					membersWithParticipantRoleSelected = true;
				}
				if (membersWithCoachAndParticipantRoleSelected && membersWithParticipantRoleSelected) {
					break;
				}
			}
			if (membersWithParticipantRoleSelected) {
				showError("error.participants.cannot.be.removed.from.group");
				return;
			} if (membersWithCoachAndParticipantRoleSelected) {
				showError("error.coaches.with.participant.role.cannot.be.removed.from.group");
				return;
			}
		}
		super.doConfirmRemoveMembers(ureq, members);
	}

	@Override
	public SearchMembersParams getSearchParams() {
		return searchParams;
	}
}
