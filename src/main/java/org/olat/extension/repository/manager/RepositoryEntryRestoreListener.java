package org.olat.extension.repository.manager;

import org.olat.core.id.Identity;
import org.olat.repository.RepositoryEntry;
import org.springframework.beans.factory.ObjectProvider;

/**
 * @author Martin Schraner
 * @since 15.5
 */
public interface RepositoryEntryRestoreListener {

	static void onAfterRestore(ObjectProvider<RepositoryEntryRestoreListener> listeners,
		RepositoryEntry repositoryEntry, Identity restoredBy) {
		listeners.orderedStream()
			.forEach(listener -> listener.onAfterRestore(repositoryEntry, restoredBy));
	}

	void onAfterRestore(RepositoryEntry repositoryEntry, Identity restoredBy);
}