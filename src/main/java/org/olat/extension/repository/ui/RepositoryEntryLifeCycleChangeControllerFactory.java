package org.olat.extension.repository.ui;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.control.WindowControl;
import org.olat.repository.RepositoryEntry;
import org.olat.repository.RepositoryEntrySecurity;
import org.olat.repository.handlers.RepositoryHandler;
import org.olat.repository.ui.RepositoryEntryLifeCycleChangeController;

/**
 * @author Martin Schraner
 * @since 15.5
 */
public interface RepositoryEntryLifeCycleChangeControllerFactory {

	RepositoryEntryLifeCycleChangeController create(
		UserRequest userRequest,
		WindowControl windowControl,
		RepositoryEntrySecurity repositoryEntrySecurity,
		RepositoryHandler repositoryHandler,
		RepositoryEntry repositoryEntry);
}
