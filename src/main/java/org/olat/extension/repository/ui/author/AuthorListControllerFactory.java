package org.olat.extension.repository.ui.author;

import org.olat.core.gui.UserRequest;
import org.olat.core.gui.components.form.flexible.impl.Form;
import org.olat.core.gui.control.WindowControl;
import org.olat.repository.model.SearchAuthorRepositoryEntryViewParams;
import org.olat.repository.ui.author.AuthorListConfiguration;
import org.olat.repository.ui.author.AuthorListController;

/**
 * @author Martin Schraner
 * @since 11.4
 */
public interface AuthorListControllerFactory {

	AuthorListController create(UserRequest ureq, WindowControl wControl, 
			SearchAuthorRepositoryEntryViewParams searchParams, AuthorListConfiguration configuration);

	AuthorListController create(UserRequest ureq, WindowControl wControl, Form rootForm, 
			SearchAuthorRepositoryEntryViewParams searchParams, AuthorListConfiguration configuration);

}
