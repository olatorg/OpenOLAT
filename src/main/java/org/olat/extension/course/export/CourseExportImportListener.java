package org.olat.extension.course.export;

import java.io.File;
import java.util.zip.ZipOutputStream;

import org.olat.course.PersistingCourseImpl;
import org.olat.course.groupsandrights.CourseGroupManager;
import org.olat.repository.RepositoryEntry;

/**
 * @author Martin Schraner
 * @since 16.2
 */
public interface CourseExportImportListener {

	void onAfterExport(PersistingCourseImpl sourceCourse, ZipOutputStream zipOutputStream);

	void onAfterImport(RepositoryEntry importedRepositoryEntry,
    		CourseGroupManager courseGroupManager, File fImportBaseDirectory);
}
