package org.olat.extension.repository.ui.list;

import java.util.Locale;
import org.olat.repository.RepositoryEntryMyView;
import org.olat.repository.ui.list.RepositoryEntryRow;
import org.springframework.stereotype.Component;

/**
 * @author Martin Schraner
 * @since 15.5
 */
@Component
public class RepositoryEntryRowFactoryImpl implements RepositoryEntryRowFactory {

	@Override
	public RepositoryEntryRow create(RepositoryEntryMyView repositoryEntryMyView, Locale locale) {
		return new RepositoryEntryRow(repositoryEntryMyView);
	}
}